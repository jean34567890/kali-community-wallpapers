# Kali Community Wallpapers

- Place in your wallpaper into `./` (Preferably as a PNG format)
- Run: `$ ./bin/rename.sh` to sort out filenames
- Run: `$ ./bin/generate-gnome.sh > debian/gnome/kali-community-wallpapers.xml`
- Update `debian/changelog`
- Run: `$ git add .; git commit -m 'My awesome wallpaper'; git push`
- Build package
